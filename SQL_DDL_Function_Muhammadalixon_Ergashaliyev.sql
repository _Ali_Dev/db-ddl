/*
1.
Create a view called "sales_revenue_by_category_qtr" that shows the film category and total sales revenue for the current quarter. 
The view should only display categories with at least one sale in the current quarter. 
The current quarter should be determined dynamically.
*/
CREATE VIEW sales_revenue_by_category_qtr AS
SELECT 
    c.name AS category_name,
    SUM(p.amount) AS total_sales_revenue
FROM 
    category c
JOIN 
    film_category fc ON c.category_id = fc.category_id
JOIN 
    film f ON fc.film_id = f.film_id
JOIN 
    inventory i ON f.film_id = i.film_id
JOIN 
    rental r ON i.inventory_id = r.inventory_id
JOIN 
    payment p ON r.rental_id = p.rental_id
WHERE 
    EXTRACT(QUARTER FROM r.rental_date) = 1 
    AND EXTRACT(YEAR FROM r.rental_date) = 2017
GROUP BY 
    c.name,
    p.amount 
HAVING 
	sum(p.amount) > 0
	
	
   drop view sales_revenue_by_category_qtr;
   select * from sales_revenue_by_category_qtr;


/*
2.
Create a query language function called "get_sales_revenue_by_category_qtr" that accepts one parameter representing 
the current quarter and returns the same result as the "sales_revenue_by_category_qtr" view.
*/

CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_quarter INT)
RETURNS TABLE(category_name VARCHAR, total_sales_revenue DECIMAL) AS $$
BEGIN
    RETURN QUERY
    SELECT 
        c.name AS category_name,
        SUM(p.amount) AS total_sales_revenue
    FROM 
        category c
    JOIN 
        film_category fc ON c.category_id = fc.category_id
    JOIN 
        film f ON fc.film_id = f.film_id
    JOIN 
        inventory i ON f.film_id = i.film_id
    JOIN 
        rental r ON i.inventory_id = r.inventory_id
    JOIN 
        payment p ON r.rental_id = p.rental_id
    WHERE 
         EXTRACT(QUARTER FROM r.rental_date) = current_quarter 
    AND EXTRACT(YEAR FROM r.rental_date) = 2017
    GROUP BY 
        c.name
    HAVING 
        SUM(p.amount) > 0;
END;
$$ LANGUAGE plpgsql;

/*
3.
Create a procedure language function called "new_movie" that takes a movie title as a parameter and inserts a new movie with the 
given title in the film table. The function should generate a new unique film ID, set the rental rate to 4.99, the rental 
duration to three days, the replacement cost to 19.99, the release year to the current year, and "language" as Klingon.
 The function should also verify that the language exists in the "language" table. 
Then, ensure that no such function has been created before; if so, replace it.
*/

CREATE OR REPLACE FUNCTION new_movie(movie_title VARCHAR)
RETURNS VOID AS $$
DECLARE
    new_film_id INT;
BEGIN
    IF NOT EXISTS (SELECT 1 FROM "language" WHERE name = 'Klingon') THEN
        INSERT INTO "language" (name) VALUES ('Klingon');
    END IF;

    IF NOT EXISTS (SELECT 1 FROM film WHERE title = movie_title) THEN
        SELECT COALESCE(MAX(film_id) + 1, 1) INTO new_film_id FROM film;

        INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
        VALUES (new_film_id, movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), 
            (SELECT language_id FROM "language" WHERE name = 'Klingon'));
    END IF;
END;
$$ LANGUAGE plpgsql;


select * from film;